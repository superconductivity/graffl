
<!-- README.md is generated from README.Rmd. Please edit that file -->

# gRaffl

<!-- badges: start -->

[![pipeline
status](https://gitlab.com/mos2-thesis/graffl/badges/main/pipeline.svg)](https://gitlab.com/mos2-thesis/graffl/pipelines)
<!-- badges: end -->

A collection of functions used in my ![masters
thesis](https://gitlab.com/mos2-thesis).

## Installation

You can install the development version of `{gRaffl}` like so:

``` r
remotes::install_gitlab("mos2-thesis/graffl")
```

or when using `{renv}`

``` r
renv::install("gitlab::mos2-thesis/graffl")
```
